# Web Sequence Diagram Remote Macro

This is a simple Confluence remote macro built using the [atlassian-connect-express](https://bitbucket.org/atlassian/atlassian-connect-express) library. This macro uses [Atlassian Connect](https://developer.atlassian.com/static/connect/docs/)'s [static content macro](https://developer.atlassian.com/static/connect/docs/modules/confluence/static-content-macro.html) module.

![flow](http://www.websequencediagrams.com/cgi-bin/cdraw?lz=Q29uZmx1ZW5jZS0-IEFkZC1vbjogU2VuZCBHRVQgcmVxdWVzdCB3aXRoIHRleHQgdG9cbnBhc3MgdG8gd2Vic2VxADgFZGlhZ3JhbXMuY29tCgBBBi0-AAkXOgBFGgoAPBctPgCBFAdSZXNwb25kAIELBmltYWdlIFVSTABpCQCBRAo6VXBsb2FkAB0HdG8gAIFfCiBwYWdlLCB0aGVuXG5zZW5kIFhIVE1MACMIYWcAgWkGYXR0YWNobWVudCBVUkw&s=vs2010)

## Get going...

Of course, for any of this to work, you'll need to have a Confluence instance handy. If you don't, you can learn about Atlassian Connect and how to run it by [visiting our documentation](https://developer.atlassian.com/static/connect/docs/).

You'll also need a copy of the `atlas-connect` toolkit:

    npm install -g atlas-connect

Then, you'll need to generate your own keys:

    atlas-connect keygen

Then, install the dependencies:

    npm install

Then,

    node app.js

(boom) you're up. If you're not, check the `config.js` to make sure you're registering to the right host.